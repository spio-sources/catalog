import { Injectable, Inject } from "@nestjs/common";
import { Repository } from "src/db/repository";
import { PESELService } from "./pesel-service";
import { Person } from "./person";

@Injectable()
export class Catalog {
    constructor(
        @Inject('Repository') private readonly db: Repository,
        @Inject('PESELService') private readonly peselService: PESELService) { }

    getPESEL(name: string, surname: string): string {
        return this.db.getPESELList(name, surname)[0];
    }

    getPESELList(name: string, surname: string): string[] {
        return this.db.getPESELList(name, surname);
    }

    addPerson(name: string, surname: string, pesel: string): void {
        if (this.peselService.verify(pesel)) {
            let p = new Person(name, surname, pesel);
            this.db.insertPerson(p);
        } else {
            throw new Error("improper PESEL");
        }
    }

    getPerson(pesel: string): Person {
        return this.db.getPerson(pesel);
    }
}